import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuFrontComponent } from './front/menu-front/menu-front.component';
import { HomeComponent } from './front/home/home.component';
import { TemplateComponent } from './front/template.component';
import { DashboardComponent } from './back/dashboard/dashboard.component';
import { FooterComponent } from './includes/footer/footer.component';

const routes: Routes = [
    { path: '', 
      component: TemplateComponent,
      children:[
        { path: 'home', component:HomeComponent  },
        { path:'menu', component:MenuFrontComponent },
       // { path: '', redirectTo:'/home', pathMatch:'full' },
       { path: '', component:HomeComponent  },
      ]
    },
    { path:'dashboard', component:DashboardComponent },
    { path:'footer', component:FooterComponent },
    { path:'menu-back', component:MenuFrontComponent },
    { path:'**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

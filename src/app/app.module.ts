import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuFrontComponent } from './front/menu-front/menu-front.component';
import { HomeComponent } from './front/home/home.component';
import { TemplateComponent } from './front/template.component';
import { DashboardComponent } from './back/dashboard/dashboard.component';
import { FooterComponent } from './includes/footer/footer.component';
import { MenuBackComponent } from './back/menu-back/menu-back.component';

import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './services/api/api.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuFrontComponent,
    HomeComponent,
    TemplateComponent,
    DashboardComponent,
    FooterComponent,
    MenuBackComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

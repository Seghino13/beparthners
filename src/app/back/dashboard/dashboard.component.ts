import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  tab1:boolean = true;
  tab2:boolean = false;
  tab3:boolean = false;

  tab2_1:boolean = true;
  tab2_2:boolean = false;

  constructor() { }

  ngOnInit() {
  }

  tabs(opt){
    if( opt == 1 ){
      this.tab1 = true;
      this.tab2 = false;
      this.tab3 = false;
    }
    if( opt == 2 ){
      this.tab1 = false;
      this.tab2 = true;
      this.tab3 = false;
    }
    if( opt == 3 ){
      this.tab1 = false;
      this.tab2 = false;
      this.tab3 = true;
    }
  }

  tabs2(opt){
    if( opt == 1 ){
      this.tab2_1 = true;
      this.tab2_2 = false; 
    }
    if( opt == 2 ){
      this.tab2_1 = false;
      this.tab2_2 = true;
    }
   
  }

}

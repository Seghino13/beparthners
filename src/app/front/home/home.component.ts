import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  BePartners = [];
  pricinTable = [];
  constructor(public _api:ApiService) { }

  ngOnInit() {

    this.BePartners = [
      {icon:'icon1.png',title:'Validar Identidad',content:'Valida la identidad de persona naturales, disminuyendo el riesgo de suplantación'},
      {icon:'icon2.png',title:'Perfilar Clientes',content:'Verificar el perfil de riesgo de tus clientes para otorgar un crédito o financiar productos'},
      {icon:'icon3.png',title:'Identificar Riesgos',content:'Analiza clientes potenciales y establece el nivel de riesgo oportunamente'},
    ];

    this.pricinTable = [
      {plan:'Prueba Gratis', valor:'17.000',cant:'1 CONSULTA GRATIS',color:'purple',items:[
            { icon:'check-purple.png' , item: '1 Consulta Propia'},
            { icon:'check-purple.png' , item: 'Vigencia'},
            { icon:'check-purple.png' , item: 'Historial de consultas'},
            { icon:'check-purple.png' , item: 'Informe descargable'},
            { icon:'check-purple.png' , item: 'Vigencia 30 días'}
        ]
      },
      {plan:'Básico', valor:'12.500',cant:'POR CONSULTA',color:'green',items:[
        { icon:'check-green.png' , item: '20 Consultas'},
        { icon:'check-green.png' , item: 'Panel de consultas'},
        { icon:'check-green.png' , item: 'Historial de consultas'},
        { icon:'check-green.png' , item: 'Informe descargable'},
        { icon:'check-green.png' , item: 'Vigencia 1 año'}
        ]
      },
      {plan:'Avanzado', valor:'5.906',cant:'POR CONSULTA',color:'purple',items:[
        { icon:'check-green.png' , item: '300 Consultas'},
        { icon:'check-green.png' , item: 'Panel de consultas'},
        { icon:'check-green.png' , item: 'Historial de consultas'},
        { icon:'check-green.png' , item: 'Informe descargable'},
        { icon:'check-green.png' , item: 'Vigencia 1 año'}
        ]
      }
    ];

    this.getPhotos();
  }

  getPhotos(){
    this._api.getPhotos().subscribe(
      ok=>{
        console.log('Objeto de http://jsonplaceholder.typicode.com/ ',ok);
      },err=>{
        console.log(err);
      }
    );
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl = 'http://jsonplaceholder.typicode.com/';
  constructor( public http: HttpClient ) { 
  }

  getPhotos(){
    let url = this.apiUrl + "photos";
    return this.http.get(url);
  }
}
